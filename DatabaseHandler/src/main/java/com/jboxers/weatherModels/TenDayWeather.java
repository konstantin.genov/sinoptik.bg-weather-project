package com.jboxers.weatherModels;

import com.jboxers.weatherDataModels.TenDayWeatherData;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TenDayWeather {
    public final static String getAllFields = "getTenDayAllFields";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<TenDayWeatherData> tenDayWeatherData = new ArrayList<>();

//    public void addWeather(int humidity, int pressure, int minimumTemperature,
//                           int maximumTemperature, double visibility, double windSpeed, String threadStart) {
//        tenDayWeatherData.add(new TenDayWeatherData(
//                new WeatherData(humidity, pressure, minimumTemperature, maximumTemperature, visibility, windSpeed, threadStart)));
//    }


    public List<TenDayWeatherData> getTenDayWeatherData() {
        return tenDayWeatherData;
    }

    public void setTenDayWeatherData(List<TenDayWeatherData> tenDayWeatherData) {
        this.tenDayWeatherData = tenDayWeatherData;
    }

    @Override
    public String toString() {
        return "Ten Day weather: " +
                "\r\n " + tenDayWeatherData.toString();
    }
}
