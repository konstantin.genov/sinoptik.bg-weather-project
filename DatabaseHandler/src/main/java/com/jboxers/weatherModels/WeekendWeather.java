package com.jboxers.weatherModels;

import com.jboxers.weatherDataModels.WeekendWeatherData;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public
class WeekendWeather {
    // String needed for JPQL simplification
    public final static String getAllFields = "getWeekendAllFields";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany(cascade = CascadeType.PERSIST)
    private List<WeekendWeatherData> weekendWeatherData = new ArrayList<>();

    public List<WeekendWeatherData> getWeekendWeatherData() {
        return weekendWeatherData;
    }

    @Override
    public String toString() {
        return "Weekend weather: " +
                "\r\n + " + weekendWeatherData.toString();
    }
}