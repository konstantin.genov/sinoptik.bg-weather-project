package com.jboxers.weatherModels;

import javax.persistence.*;


@NamedQueries(value = {
        // Queries all CurrentWeather objects
        @NamedQuery(name = CurrentWeather.getAllFields, query = "SELECT f.currentWeather FROM Forecast f"),
        @NamedQuery(name = CurrentWeather.getFromDates, query = "SELECT f.currentWeather FROM Forecast f WHERE f.threadStart = :threadStart"),

        // Queries for HourlyWeather objects
        @NamedQuery(name = HourlyWeather.getAllFields, query = "SELECT f.hourlyWeather.hourlyWeatherData FROM Forecast f where f.threadStart = :threadStart"),
//        @NamedQuery(name = HourlyWeather.getFromDate, query = "SELECT f.hourlyWeather.hourlyWeatherData FROM Forecast f where f.threadStart = :threadStart"),
        @NamedQuery(name = HourlyWeather.getFromDate, query = "SELECT f.hourlyWeather FROM Forecast f where f.threadStart = :threadStart"),

        // Queries for WeekendWeather objects
        @NamedQuery(name = WeekendWeather.getAllFields, query = "SELECT f.weekendWeather FROM Forecast f"),


        // Queries for FiveDayWeather objects
        @NamedQuery(name = FiveDayWeather.getAllFields, query = "SELECT f.fiveDayWeather.fiveDayWeatherData FROM Forecast f"),

        // Queries for TenDayWeather Objects
        @NamedQuery(name = TenDayWeather.getAllFields, query = "SELECT f.tenDayWeather FROM Forecast f"),

        @NamedQuery(name = Forecast.findAll, query = "SELECT f FROM Forecast f"),

        @NamedQuery(name = Forecast.findAllByDate, query = "SELECT f from Forecast f where f.threadStart = :threadStart")


})
@Entity
public class Forecast {
    public final static String findAll = "getAllForecasts";
    public final static String findAllByDate = "getAllForDate";

    @Id
    @GeneratedValue
    private int id;
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }


    public CurrentWeather getCurrentWeather() {
        return currentWeather;
    }

    public void setCurrentWeather(CurrentWeather currentWeather) {
        this.currentWeather = currentWeather;
    }

    public HourlyWeather getHourlyWeather() {
        return hourlyWeather;
    }

    public void setHourlyWeather(HourlyWeather hourlyWeather) {
        this.hourlyWeather = hourlyWeather;
    }

    public WeekendWeather getWeekendWeather() {
        return weekendWeather;
    }

    public void setWeekendWeather(WeekendWeather weekendWeather) {
        this.weekendWeather = weekendWeather;
    }

    public FiveDayWeather getFiveDayWeather() {
        return fiveDayWeather;
    }

    public void setFiveDayWeather(FiveDayWeather fiveDayWeather) {
        this.fiveDayWeather = fiveDayWeather;
    }

    public TenDayWeather getTenDayWeather() {
        return tenDayWeather;
    }

    public void setTenDayWeather(TenDayWeather tenDayWeather) {
        this.tenDayWeather = tenDayWeather;
    }

    public String getThreadStart() {
        return threadStart;
    }

    public void setThreadStart(String threadStart) {
        this.threadStart = threadStart;
    }

    @OneToOne(cascade = CascadeType.ALL)
    private CurrentWeather currentWeather;

    @OneToOne(cascade = CascadeType.ALL)
    private HourlyWeather hourlyWeather;

    @OneToOne(cascade = CascadeType.ALL)
    private WeekendWeather weekendWeather;

    @OneToOne(cascade = CascadeType.ALL)
    private FiveDayWeather fiveDayWeather;

    @OneToOne(cascade = CascadeType.ALL)
    private TenDayWeather tenDayWeather;

    private String threadStart;


    public Forecast(CurrentWeather currentWeather,
                    HourlyWeather hourlyWeather,
                    WeekendWeather weekendWeather,
                    FiveDayWeather fiveDayWeather,
                    TenDayWeather tenDayWeather,
                    String threadStart,
                    String city) {
        this.currentWeather = currentWeather;
        this.hourlyWeather = hourlyWeather;
        this.weekendWeather = weekendWeather;
        this.fiveDayWeather = fiveDayWeather;
        this.tenDayWeather = tenDayWeather;
        this.threadStart = threadStart;
        this.city = city;
    }

    public Forecast() {
    }


    @Override
    public String toString() {
        return "Forecast{" +
                "id=" + id +
                ", currentWeather=" + currentWeather +
                ", hourlyWeather=" + hourlyWeather +
                ", weekendWeather=" + weekendWeather +
                ", fiveDayWeather=" + fiveDayWeather +
                ", tenDayWeather=" + tenDayWeather +
                '}';
    }
}
