package com.jboxers.weatherModels;

import com.jboxers.weatherDataModels.FiveDayWeatherData;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class FiveDayWeather {
    // String needed for JPQL
    public final static String getAllFields = "getFiveDayAllFields";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @OneToMany(cascade = CascadeType.PERSIST)
    private
    List<FiveDayWeatherData> fiveDayWeatherData = new ArrayList<>();

    public List<FiveDayWeatherData> getFiveDayWeatherData() {
        return fiveDayWeatherData;
    }

    //    public boolean addWeather(int humidity, int pressure, int minimumTemperature,
//                              int maximumTemperature, double visibility, double windSpeed, String threadStart) {
//        return fiveDayWeatherData.add(new FiveDayWeatherData(
//                new WeatherData(humidity, pressure, minimumTemperature, maximumTemperature, visibility, windSpeed, threadStart)));
//    }


    @Override
    public String toString() {
        return "Five Day weather: " +
                "\r\n " + fiveDayWeatherData.toString();
    }
}
