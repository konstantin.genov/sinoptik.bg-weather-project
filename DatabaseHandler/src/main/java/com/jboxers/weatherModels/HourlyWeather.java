package com.jboxers.weatherModels;

import com.jboxers.weatherDataModels.HourlyWeatherData;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class HourlyWeather {
    //String for JPQL simplicity
    public final static String getAllFields = "getHourlyAllFields";
    public final static String getFromDate = "getFromDate";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @OneToMany(cascade = CascadeType.PERSIST)
    private List<HourlyWeatherData> hourlyWeatherData = new ArrayList<>();

    public HourlyWeather() {
    }

    public HourlyWeather(List<HourlyWeatherData> hourlyWeatherData) {
        this.hourlyWeatherData = hourlyWeatherData;
    }


    public List<HourlyWeatherData> getHourlyWeatherData() {
        return hourlyWeatherData;
    }

    @Override
    public String toString() {
        return "Hourly weather: " +
                "\r\n " + hourlyWeatherData.toString();
    }
}
