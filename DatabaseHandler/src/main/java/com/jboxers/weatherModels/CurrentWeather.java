package com.jboxers.weatherModels;

import com.jboxers.weatherDataModels.WeatherData;

import javax.persistence.*;

/**
 *
 */
@Entity
//@NamedQuery(name = "CurrentWeather.temperature", query = "SELECT dw.id FROM CurrentWeather  dw")
public class CurrentWeather {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    // Strings for queries used in the Forecast Object
    public final static String getFromDates = "getFromDates";
    public final static String getAllFields = "getCurrentAllFields";


    @OneToOne(cascade = CascadeType.PERSIST)
    private
    WeatherData dailyWeatherData;

    public CurrentWeather() {
    }

    public CurrentWeather(WeatherData dailyWeatherData) {
        this.dailyWeatherData = dailyWeatherData;
    }

    @Override
    public String toString() {
        return "Current weather: " +
                "\r\n " + dailyWeatherData.toString();

    }

    public WeatherData getDailyWeatherData() {
        return dailyWeatherData;
    }
}
