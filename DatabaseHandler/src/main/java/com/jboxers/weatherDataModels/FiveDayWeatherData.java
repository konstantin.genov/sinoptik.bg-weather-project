package com.jboxers.weatherDataModels;

import javax.persistence.*;

@Entity
public class FiveDayWeatherData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne(cascade = CascadeType.PERSIST)
    private WeatherData weatherData;
    private String day;

    public WeatherData getWeatherData() {
        return weatherData;
    }

    public FiveDayWeatherData(String day, WeatherData weatherData) {
        this.day = day;
        this.weatherData = weatherData;
    }

    public FiveDayWeatherData() {
    }

    @Override
    public String toString() {
        return weatherData.toString();
    }
}



