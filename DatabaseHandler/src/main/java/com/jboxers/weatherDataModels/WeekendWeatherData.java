package com.jboxers.weatherDataModels;

import javax.persistence.*;

@Entity
public class WeekendWeatherData {

    @OneToOne(cascade = CascadeType.PERSIST)

    private WeatherData weatherData;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String day;

    public WeekendWeatherData(WeatherData weatherData, String day) {
        this.weatherData = weatherData;
        this.day = day;
    }

    public WeekendWeatherData() {
    }

    @Override
    public String toString() {
        return weatherData.toString();
    }
}
