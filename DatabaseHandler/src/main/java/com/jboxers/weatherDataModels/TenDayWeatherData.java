package com.jboxers.weatherDataModels;

import javax.persistence.*;

@Entity
public class TenDayWeatherData {
    // String for JPQL simplicity

    @OneToOne(cascade = CascadeType.PERSIST)
    private WeatherData weatherData;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String day;


    public TenDayWeatherData(WeatherData weatherData, String day) {
        this.weatherData = weatherData;
        this.day = day;
    }

    public TenDayWeatherData() {
    }

    @Override
    public String toString() {
        return weatherData.toString();
    }
}
