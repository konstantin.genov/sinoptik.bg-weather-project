package com.jboxers.weatherDataModels;

import javax.persistence.*;

@Entity
public class WeatherData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int temperature;
    private int minimumTemperature;
    private int maximumTemperature;
    private int cloudiness;
    private int humidity;
    private int chanceToRain;
    private double quantityOfRain;
    private int chanceOfStorm;
    private double windSpeed;

    private String threadStart;
    private String date;


    public WeatherData() {
    }

    public WeatherData(int temperature, int cloudiness, int humidity, int chanceToRain, double quantityOfRain, int chanceOfStorm, double windSpeed, String threadStart) {
        this.temperature = temperature;
        this.cloudiness = cloudiness;
        this.humidity = humidity;
        this.chanceToRain = chanceToRain;
        this.quantityOfRain = quantityOfRain;
        this.chanceOfStorm = chanceOfStorm;
        this.windSpeed = windSpeed;
        this.threadStart = threadStart;
    }

    public WeatherData(int minimumTemperature, int maximumTemperature, int cloudiness, int chanceToRain, double quantityOfRain, int chanceOfStorm, double windSpeed, String date, String threadStart) {
        this.minimumTemperature = minimumTemperature;
        this.maximumTemperature = maximumTemperature;
        this.cloudiness = cloudiness;
        this.chanceToRain = chanceToRain;
        this.quantityOfRain = quantityOfRain;
        this.chanceOfStorm = chanceOfStorm;
        this.windSpeed = windSpeed;
        this.date = date;
        this.threadStart = threadStart;
    }


    public int getId() {
        return id;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getMinimumTemperature() {
        return minimumTemperature;
    }

    public void setMinimumTemperature(int minimumTemperature) {
        this.minimumTemperature = minimumTemperature;
    }

    public int getMaximumTemperature() {
        return maximumTemperature;
    }

    public void setMaximumTemperature(int maximumTemperature) {
        this.maximumTemperature = maximumTemperature;
    }

    public int getCloudiness() {
        return cloudiness;
    }

    public void setCloudiness(int cloudiness) {
        this.cloudiness = cloudiness;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getChanceToRain() {
        return chanceToRain;
    }

    public void setChanceToRain(int chanceToRain) {
        this.chanceToRain = chanceToRain;
    }

    public double getQuantityOfRain() {
        return quantityOfRain;
    }

    public void setQuantityOfRain(double quantityOfRain) {
        this.quantityOfRain = quantityOfRain;
    }

    public int getChanceOfStorm() {
        return chanceOfStorm;
    }

    public void setChanceOfStorm(int chanceOfStorm) {
        this.chanceOfStorm = chanceOfStorm;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getThreadStart() {
        return threadStart;
    }

    public void setThreadStart(String threadStart) {
        this.threadStart = threadStart;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    //    public abstract boolean addWeather(int humidity, int pressure, int minimumTemperature,
//                                       int maximumTemperature, double visibility, double windSpeed, String threadStart);


    @Override
    public String toString() {

        if (date == null) {
            return "Temperature: " + temperature + "" +
                    "\r\n Cloudiness: " + cloudiness +
                    "\r\n Humidity: " + humidity +
                    "\r\n Chance to rain: " + chanceToRain + "%" +
                    "\r\n Quantity of rain: " + quantityOfRain + " mm" +
                    "\r\n Chance of storming: " + chanceOfStorm + "%" +
                    "\r\n Wind speed: " + windSpeed +
                    "\r\n This object was captured by the thread on " + threadStart
                    + "\r\n";
        } else {
            return "Date: " + date +
                    "\r\n Minimum temperature: " + minimumTemperature +
                    "\r\n Maximum temperature: " + maximumTemperature +
                    "\r\n Cloudiness: " + cloudiness +
                    "\r\n Chance to rain: " + chanceToRain + "%" +
                    "\r\n Quantity of rain: " + quantityOfRain + " mm" +
                    "\r\n Chance of storming: " + chanceOfStorm + "%" +
                    "\r\n Wind speed: " + windSpeed +
                    "\r\n This object was captured by the thread on " + threadStart +
                    "\r\n";
        }

        //int minimumTemperature, int maximumTemperature, int cloudiness, int chanceToRain, double quantityOfRain, int chanceOfStorm, double windSpeed, String date, String threadStart
    }
}
