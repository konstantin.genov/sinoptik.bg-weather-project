package com.jboxers.weatherDataModels;

import javax.persistence.*;

@Entity
public class HourlyWeatherData {
    // String needed for JPQL simplification
    public final static String getAllFields = "getHourlyAllFields";

    private String hour;
    private int pressure;


    @OneToOne(cascade = CascadeType.PERSIST)
    private WeatherData weatherData;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    public HourlyWeatherData(String hour, int pressure, WeatherData weatherData) {
        this.hour = hour;
        this.pressure = pressure;
        this.weatherData = weatherData;
    }

    public HourlyWeatherData() {
    }

    public static String getGetAllFields() {
        return getAllFields;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public WeatherData getWeatherData() {
        return weatherData;
    }

    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Hourly weather for: " + hour +
                "\r\n " + weatherData.toString();
    }
}
