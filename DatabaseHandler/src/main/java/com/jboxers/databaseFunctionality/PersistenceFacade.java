package com.jboxers.databaseFunctionality;


import com.jboxers.weatherDataModels.*;
import com.jboxers.weatherModels.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PersistenceFacade {
    private static final String PERSISTENCE_UNIT_NAME = "my-app";
    private EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    private EntityManager em = factory.createEntityManager();


    public List<CurrentWeather> getCurrentWeather() {
        return em.createNamedQuery(CurrentWeather.getAllFields, CurrentWeather.class).getResultList();
    }


    public List<CurrentWeather> getCurrentWeatherFromDate(String threadStart) {
        return em.createNamedQuery(CurrentWeather.getFromDates, CurrentWeather.class)
                .setParameter("threadStart", threadStart)
                .getResultList();
    }

    // Methods for HourlyWeather objects
    public List<HourlyWeatherData> getHourlyWeatherData() {
        return em.createNamedQuery(HourlyWeather.getAllFields, HourlyWeatherData.class)
                .getResultList();
    }

    public List<HourlyWeatherData> getHourlyWeatherDataFromDate(String threadStart) {
        return em.createNamedQuery(HourlyWeather.getAllFields, HourlyWeatherData.class)
                .setParameter("threadStart", threadStart)
                .getResultList();
    }


    // Methods for HourlyWeather objects
    public List<HourlyWeather> getHourlyWeatherFromDate(String threadStart) {
        return em.createNamedQuery(HourlyWeather.getFromDate, HourlyWeather.class)
                .setParameter("threadStart", threadStart)
                .getResultList();
    }

    // Methods for WeekendWeather objects
    public List<WeekendWeatherData> getWeekendWeather() {
        return em.createNamedQuery(WeekendWeather.getAllFields, WeekendWeatherData.class).getResultList();
    }

    // Methods for FiveDayWeather objects
    public List<FiveDayWeatherData> getFiveDayWeather() {
        return em.createNamedQuery(FiveDayWeather.getAllFields, FiveDayWeatherData.class).getResultList();
    }

    // Methods for TenDayWeather objects
    public List<TenDayWeatherData> getTenDayWeather() {
        return em.createNamedQuery(TenDayWeather.getAllFields, TenDayWeatherData.class).getResultList();
    }

    public List<Forecast> getForecastsByDate(String date) {
        return em.createNamedQuery(Forecast.findAllByDate, Forecast.class)
                .setParameter("date", date)
                .getResultList();
    }


}
