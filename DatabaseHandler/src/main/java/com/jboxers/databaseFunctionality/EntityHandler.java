package com.jboxers.databaseFunctionality;

import com.jboxers.weatherModels.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * This class handles the database operations such as saving the weather
 */
public class EntityHandler {
    private static final String PERSISTENCE_UNIT_NAME = "my-app";

    /**
     * @param currentWeather - Current Weather object provided by the RegexParser
     * @param hourlyWeather  - Hourly Weather object provided by the RegexParser
     * @param weekendWeather - Weekend Weather object provided by the RegexParser
     * @param fiveDayWeather - Five Day Weather object provided by the RegexParser
     * @param tenDayWeather  - Ten Day Weather object provided by the RegexParser
     * @param threadStart    - the time when the thread has started
     * @param city           - city for which weather information is provided
     */
    public void saveWeather(CurrentWeather currentWeather, HourlyWeather hourlyWeather, WeekendWeather weekendWeather,
                            FiveDayWeather fiveDayWeather, TenDayWeather tenDayWeather, String threadStart, String city) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        String[] threadToDate = threadStart.split(" ");

        Forecast forecast = new Forecast(currentWeather, hourlyWeather, weekendWeather, fiveDayWeather, tenDayWeather, threadToDate[0], city);

        entityManager.getTransaction().begin();
        entityManager.persist(currentWeather);
        entityManager.persist(hourlyWeather);
        entityManager.persist(fiveDayWeather);
        entityManager.persist(tenDayWeather);
        entityManager.persist(hourlyWeather);
        entityManager.persist(weekendWeather);
        entityManager.persist(forecast);
        entityManager.getTransaction().commit();
    }
}
