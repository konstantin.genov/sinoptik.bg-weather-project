package com.jboxers.weatherDTOs;

public class WeekendWeatherDTO {
    private int minimumTemperature;
    private int maximumTemperature;
    private String threadStart;

    public WeekendWeatherDTO() {
    }

    public int getMinimumTemperature() {
        return minimumTemperature;
    }

    public void setMinimumTemperature(int minimumTemperature) {
        this.minimumTemperature = minimumTemperature;
    }

    public int getMaximumTemperature() {
        return maximumTemperature;
    }

    public void setMaximumTemperature(int maximumTemperature) {
        this.maximumTemperature = maximumTemperature;
    }

    public String getThreadStart() {
        return threadStart;
    }

    public void setThreadStart(String threadStart) {
        this.threadStart = threadStart;
    }
}
