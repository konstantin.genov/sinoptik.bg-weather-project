package com.jboxers.weatherDTOs;

import com.jboxers.databaseFunctionality.PersistenceFacade;
import com.jboxers.weatherDataModels.FiveDayWeatherData;
import com.jboxers.weatherDataModels.HourlyWeatherData;
import com.jboxers.weatherDataModels.TenDayWeatherData;
import com.jboxers.weatherDataModels.WeekendWeatherData;
import com.jboxers.weatherModels.CurrentWeather;
import com.jboxers.weatherModels.HourlyWeather;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DTOMapper {

    private static DTOMapper dtoMapper;
    private static Mapper mapper = new DozerBeanMapper();

    static {
        dtoMapper = new DTOMapper();
    }

    private static PersistenceFacade persistenceFacade = new PersistenceFacade();

    public static List<CurrentWeatherDTO> getCurrentWeatherList(String threadStart, String hourCheck) {
        List<CurrentWeatherDTO> currentWeatherDTOS = new ArrayList<>();
        List<CurrentWeather> queryResults = persistenceFacade.getCurrentWeatherFromDate(threadStart);

        for (CurrentWeather queryResult : queryResults) {
            if (queryResult.getDailyWeatherData().getThreadStart().equals(hourCheck)) {
                break;
            }
            String[] split = queryResult.getDailyWeatherData().getThreadStart().split(":");
            System.out.println(queryResult.getDailyWeatherData().getThreadStart());

            currentWeatherDTOS.add(new CurrentWeatherDTO(queryResult.getDailyWeatherData().getTemperature(), split[0] + ":00"));

        }

        return currentWeatherDTOS;
    }

    /**
     * This method will return a list of DTOs which hold the comparisons between CurrentWeather(actual temperature recorded)
     * and HourlyWeatherData for the date provided. It will compare the actual temperature with all of the HourlyWeather's entries
     * for that hour.
     *
     * @param date - enter the date for which you would like to fetch a forecast
     * @return - returns a ForecastDTO List
     */
    public static List<ForecastDTO> getCurrentVsHourlyWeatherForecast(String date) {
        List<HourlyWeather> hourlyWeathers = persistenceFacade.getHourlyWeatherFromDate(date);
        List<CurrentWeather> currentWeathers = persistenceFacade.getCurrentWeatherFromDate(date);
        List<ForecastDTO> currentWeatherForecastDTOS = new ArrayList<>();
        String[] currentWeatherHours = new String[currentWeathers.size()]; // need an array of Strings so we can loop through the hours
        int minTemperature;
        int maxTemperature;

        for (int i = 0; i < currentWeathers.size(); i++) {
            // CurrentWeather doesn't hold a time property, so we need to substring the entire threadStart string
            String substring = currentWeathers.get(i).
                    getDailyWeatherData().
                    getThreadStart()
                    .substring(11, 13); // this is the location of the first two digits of the hour - "13:12:22" => 13
            currentWeatherHours[i] = substring + ":00";  //combine so we can get an actual hour => 13 + :00 => 13:00

            List<Integer> storedTemperaturesForHour = new ArrayList<>();
            int matches = 0; // this integer will count the matches between CurrentWeather.getHour and HourlyWeather.HourlyWeatherData.getHour

            int actualTemperature = currentWeathers.get(i)
                    .getDailyWeatherData()
                    .getTemperature();

            for (HourlyWeather currentHourlyWeather : hourlyWeathers) {

                for (HourlyWeatherData weatherData : currentHourlyWeather.getHourlyWeatherData()) {
                    String hourlyWeatherHour = weatherData.getHour();
                    String currentWeatherHour = currentWeatherHours[i];
                    if (hourlyWeatherHour.equals(currentWeatherHour)) {
                        matches++;
                        storedTemperaturesForHour.add(weatherData.getWeatherData().getTemperature());
                        break;
                    }
                }
            }
            if (storedTemperaturesForHour.size() == matches) { // array must be populated before we extrapolate min/max temperature values
                minTemperature = Collections.min(storedTemperaturesForHour);
                maxTemperature = Collections.max(storedTemperaturesForHour);
                currentWeatherForecastDTOS.add(new ForecastDTO(actualTemperature, minTemperature, maxTemperature, currentWeatherHours[i]));
            }
        }
        return currentWeatherForecastDTOS;
    }

    public static List<ForecastDTO> getCurrentVsFivedayForecast(String date) {
        List<FiveDayWeatherData> fiveDayWeatherData = persistenceFacade.getFiveDayWeather();
        List<CurrentWeather> currentWeathers = persistenceFacade.getCurrentWeatherFromDate("24/07/2019");
        List<ForecastDTO> forecastDTOS = new ArrayList<>();
        String[] currentWeatherHours = new String[currentWeathers.size()]; // need an array of Strings so we can loop through the hours
        int calculateMinTemp;
        int calculateMaxTEmp;

        for (int i = 0; i < 16; i++) {
            List<Integer> maxTemperature = new ArrayList<>();
            List<Integer> minTemperature = new ArrayList<>();
            int actualTemperature = currentWeathers.get(i).getDailyWeatherData().getTemperature();
            String substring = currentWeathers.get(i).
                    getDailyWeatherData().
                    getThreadStart()
                    .substring(11, 13); // this is the location of the first two digits of the hour - "13:12:22" => 13
            currentWeatherHours[i] = substring + ":00";

            for (FiveDayWeatherData fiveDay : fiveDayWeatherData
            ) {
                if (fiveDay.getWeatherData().getDate().equals(date))
                    maxTemperature.add(fiveDay.getWeatherData().getMaximumTemperature());
                minTemperature.add(fiveDay.getWeatherData().getMinimumTemperature());
            }
            calculateMinTemp = Collections.min(minTemperature);
            calculateMaxTEmp = Collections.max(maxTemperature);
            forecastDTOS.add(new ForecastDTO(actualTemperature, calculateMinTemp, calculateMaxTEmp, currentWeatherHours[i]));
        }
        return forecastDTOS;
    }

    public static List<HourlyWeatherDTO> getHourlyWeatherList(String threadStart) {

        List<HourlyWeatherDTO> hourlyWeatherDTOS = new ArrayList<>();
        List<HourlyWeatherData> hourlyWeather = persistenceFacade.getHourlyWeatherDataFromDate(threadStart);

        for (int i = 0; i < 23; i++) {
            hourlyWeatherDTOS.add(new HourlyWeatherDTO(hourlyWeather.get(i).getWeatherData().getTemperature(), hourlyWeather.get(i).getHour()));

        }
        return hourlyWeatherDTOS;
    }

    public static List<HourlyWeatherData> hourlyWeatherDataFromDate(String threadStart) {
        return persistenceFacade.getHourlyWeatherDataFromDate(threadStart);
    }

    public static List<HourlyWeather> hourlyWeather(String threadStart) {
        return persistenceFacade.getHourlyWeatherFromDate(threadStart);
    }


    public static List<WeekendWeatherDTO> getWeekendWeatherList(String threadStart) {
        List<WeekendWeatherDTO> weekendWeatherDTOS = new ArrayList<>();
        List<WeekendWeatherData> queryResults = persistenceFacade.getWeekendWeather();
        for (WeekendWeatherData weekendWeather : queryResults
        ) {

            weekendWeatherDTOS.add(mapper.map(weekendWeather, WeekendWeatherDTO.class));
        }
        return weekendWeatherDTOS;
    }

    public static List<FiveDayWeatherDTO> getFiveDayWeatherList(String forDate) {
        List<FiveDayWeatherDTO> fiveDayWeatherDTOS = new ArrayList<>();
        List<FiveDayWeatherData> queryResults = persistenceFacade.getFiveDayWeather();
        for (FiveDayWeatherData queryResult : queryResults) {
            if (queryResult.getWeatherData().getDate().equals(forDate)) {

                fiveDayWeatherDTOS.add(new FiveDayWeatherDTO(queryResult.getWeatherData().getMinimumTemperature(),
                        queryResult.getWeatherData().getMaximumTemperature(), queryResult.getWeatherData().getDate()));
            }
        }
        return fiveDayWeatherDTOS;
    }

    public static List<TenDayWeatherDTO> getTenDayWeatherList(String threadStart) {
        List<TenDayWeatherDTO> tenDayWeatherDTOS = new ArrayList<>();
        List<TenDayWeatherData> queryResults = persistenceFacade.getTenDayWeather();
        for (TenDayWeatherData tenDayWeather : queryResults
        ) {
            tenDayWeatherDTOS.add(mapper.map(tenDayWeather, TenDayWeatherDTO.class));
        }
        return tenDayWeatherDTOS;
    }

}
