package com.jboxers.weatherDTOs;

public class TenDayWeatherDTO {

    private int maximumTemperature;
    private int minimumTemperature;
    private double windSpeed;
    private String threadStart;

    public TenDayWeatherDTO() {
    }

    public int getMaximumTemperature() {
        return maximumTemperature;
    }

    public void setMaximumTemperature(int maximumTemperature) {
        this.maximumTemperature = maximumTemperature;
    }

    public int getMinimumTemperature() {
        return minimumTemperature;
    }

    public void setMinimumTemperature(int minimumTemperature) {
        this.minimumTemperature = minimumTemperature;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getThreadStart() {
        return threadStart;
    }

    public void setThreadStart(String threadStart) {
        this.threadStart = threadStart;
    }
}
