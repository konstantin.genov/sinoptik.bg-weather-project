package com.jboxers.weatherDTOs;

public class CurrentWeatherDTO {

    private int temperature;
    private String hour;

    public CurrentWeatherDTO() {
    }

    public CurrentWeatherDTO(int temperature, String hour) {
        this.temperature = temperature;
        this.hour = hour;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getHour() {
        return hour;
    }


    @Override
    public String toString() {
        return "CurrentWeatherDTO{" +
                "temperature=" + temperature +
                ", hour='" + hour + '\'' +
                '}';
    }
}
