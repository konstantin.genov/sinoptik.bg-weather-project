package com.jboxers.weatherDTOs;

public class FiveDayWeatherDTO {
    private int minimumDegrees;
    private int maximumDegrees;
    private String threadStart;

    public FiveDayWeatherDTO() {
    }

    public FiveDayWeatherDTO(int minimumDegrees, int maximumDegrees, String threadStart) {
        this.minimumDegrees = minimumDegrees;
        this.maximumDegrees = maximumDegrees;
        this.threadStart = threadStart;
    }

    public int getMinimumDegrees() {
        return minimumDegrees;
    }

    public void setMinimumDegrees(int minimumDegrees) {
        this.minimumDegrees = minimumDegrees;
    }

    public int getMaximumDegrees() {
        return maximumDegrees;
    }

    public void setMaximumDegrees(int maximumDegrees) {
        this.maximumDegrees = maximumDegrees;
    }


    public String getThreadStart() {
        return threadStart;
    }

    public void setThreadStart(String threadStart) {
        this.threadStart = threadStart;
    }

    @Override
    public String toString() {
        return "FiveDayWeatherDTO{" +
                "minimumDegrees=" + minimumDegrees +
                ", maximumDegrees=" + maximumDegrees +
                ", threadStart='" + threadStart + '\'' +
                '}';
    }
}
