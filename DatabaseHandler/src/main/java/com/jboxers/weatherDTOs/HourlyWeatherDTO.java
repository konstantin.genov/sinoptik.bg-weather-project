package com.jboxers.weatherDTOs;

public class HourlyWeatherDTO {
    private int temperature;
    private String hour;

    public HourlyWeatherDTO() {
    }

    public HourlyWeatherDTO(int temperature, String hour) {
        this.temperature = temperature;
        this.hour = hour;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }


    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        return "Hour: " + hour +
                "\n Temperature: " + temperature;
    }
}
