package com.jboxers.weatherDTOs;

public class ForecastDTO {
    private int actualTemperature;
    private int minTemperature;
    private int maxTemperature;
    private String hour;

    public ForecastDTO() {
    }

    public ForecastDTO(int actualTemperature, int minTemperature, int maxTemperature, String hour) {
        this.actualTemperature = actualTemperature;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
        this.hour = hour;
    }

    public int getActualTemperature() {
        return actualTemperature;
    }

    public void setActualTemperature(int actualTemperature) {
        this.actualTemperature = actualTemperature;
    }

    public int getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(int minTemperature) {
        this.minTemperature = minTemperature;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        return "ForecastDTO{" +
                "actualTemperature=" + actualTemperature +
                ", minTemperature=" + minTemperature +
                ", maxTemperature=" + maxTemperature +
                ", hour='" + hour + '\'' +
                '}';
    }
}
