<%--
  Created by IntelliJ IDEA.
  User: carnivore
  Date: 7/21/19
  Time: 3:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>jBoxers Weather Project</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>


</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="#">jBoxers</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="http://localhost:8080/WebService_war/">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost:8080/WebService_war/hourlyWeather.jsp">Hourly Weather</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost:8080/WebService_war/forecastWeather.jsp">Forecast
                        Weather </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="forecastFiveDayWeather.jsp">Forecast Test</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 class="mt-5">Hourly Weather Chart</h1>
            <p class="lead">This is a visualization of the actual temperatures for 24.07.2019 compared with the minimum
                and maximum predicted by Sinoptik.bg </p>
            <canvas id="canvas" width="1000" height="400"></canvas>

            <script src="node_modules/chart.js/dist/Chart.bundle.min.js"></script>
            <script>
                let hours = [];
                let actualTemps = [];
                let minTemps = [];
                let maxTemps = [];
                $.ajax({
                    method: 'GET',
                    url: "http://localhost:8080/WebService_war/rest/weather/forecast",

                    success: function (result) {
                        result.map(x => {
                            hours.push(x.hour);
                            actualTemps.push(x.actualTemperature);
                            minTemps.push(x.minTemperature);
                            maxTemps.push(x.maxTemperature);
                            var ctx = canvas.getContext('2d');

                            var config = {
                                type: 'line',
                                data: {
                                    labels: hours,
                                    datasets: [{
                                        label: 'Actual Temperature',
                                        data: actualTemps,
                                        fill: false,
                                        backgroundColor: 'rgba(54, 162, 235, 1)',
                                        borderColor: 'rgba(54, 162, 235, 1)',
                                    }, {
                                        label: 'Forecast Maximum temperature',
                                        data: maxTemps,  //set other axisdata
                                        fill: false,
                                        borderColor: 'rgb(226, 38, 54)',
                                        backgroundColor: 'rgb(226, 38, 54)'
                                    }, {
                                        label: 'Forecast Minimum Temperature',
                                        data: minTemps,
                                        fill: false,
                                        backgroundColor: 'rgb(1, 173, 98)',
                                        borderColor: 'rgb(1, 173, 98)',
                                    }]
                                }
                            };
                            var chart = new Chart(ctx, config);
                        });
                        console.log(actualTemps, hours)
                    }

                })
                ;


            </script>
            <ul class="list-unstyled">
                <li>Bootstrap 4.3.1</li>
                <li>jQuery 3.4.1</li>
            </ul>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->

<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
