package com.jboxers;


import com.jboxers.weatherDTOs.DTOMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/weather")
@ApplicationPath("/rest")
public class RestService extends Application {

    @GET
    @Path("/hourly") //producer for XML?
    @Produces(MediaType.APPLICATION_JSON)
    public Response hourlyWeatherUntilHour() {
        return Response.status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers",
                        "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Methods",
                        "GET")
                .entity(DTOMapper.getHourlyWeatherList("24/07/2019"))
                .build();

    }

    @GET
    @Path("/xml")
    @Produces(MediaType.APPLICATION_XML)
    public XMLDisplay hourlyWeatherXML() {
        XMLDisplay xmlDisplay = new XMLDisplay();
        xmlDisplay.setMessage("Hello there");
        xmlDisplay.setSecondMessage("I am in XML!");
        return xmlDisplay;
    }

    @GET
    @Path("/forecast")
    @Produces(MediaType.APPLICATION_JSON)
    public Response forecastData() {
        return Response.status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers",
                        "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Methods",
                        "GET")
                .entity(DTOMapper.getCurrentVsHourlyWeatherForecast("24/07/2019"))
                .build();

    }

    @GET
    @Path("/forecastfive")
    @Produces(MediaType.APPLICATION_JSON)
    public Response forecastDataFiveDay() {
        return Response.status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers",
                        "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Methods",
                        "GET")
                .entity(DTOMapper.getCurrentVsFivedayForecast("24.07.2019"))
                .build();

    }
    
}
