package com.jboxers.regexTesting;

import com.jboxers.HTTPFunctionality.HTTPConnector;
import com.jboxers.customExceptions.BadRequestException;
import com.jboxers.regexFunctionality.RegexParser;
import com.jboxers.weatherModels.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static junit.framework.TestCase.assertTrue;


public class RegexParserTest {
    private RegexParser parser = new RegexParser();
    private final String city = "SofIa"; //lower case test
    private String threadStart = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
    // The current weather parser fetches and returns a single <CurrentWeather> object
    // Expected temperature is hardcoded from the URL provided below
    @Test
    public void getCurrentWeather() throws Exception {
        //Prepare
        //URL for Sofia Current - https://www.sinoptik.bg/sofia-bulgaria-100727011
        Integer expectedTemperature = 21;
        String weatherType = "current";
        CurrentWeather currentWeather;
        currentWeather = parser.getCurrentWeather(HTTPConnector.connect("sofia", weatherType), threadStart);


        Integer actualTemperature = currentWeather.getDailyWeatherData().getTemperature();

        Assert.assertEquals(expectedTemperature, actualTemperature);
        System.out.println("---- Testing Current Weather Parser ----");
        System.out.println("Expected Temperature: " + expectedTemperature + "\nActual temperature: " + actualTemperature);
        System.out.println("================================");
        System.out.println();
    }

    // The HourlyWeather is an object which stores a list of 23 <HourlyWeatherData> objects
    // If the size is not 23, something went wrong
    @Test
    public void getHourlyWeather() throws Exception {
        //Prepare:

        HourlyWeather hourlyWeather = new HourlyWeather();
        int sizeBeforePopulation = hourlyWeather.getHourlyWeatherData().size();
        String weatherType = "hourly";
        int numberOfObjects = 22;


        hourlyWeather = parser.getHourlyWeather(HTTPConnector.connect(city, weatherType), threadStart);

        // Assert
        Integer expectedSizeLow = sizeBeforePopulation + numberOfObjects; // the list of objects should contain 22 entries / 23 entries depending on the change of Sinotpik
        Integer expectedSizeHigh = sizeBeforePopulation + numberOfObjects + 1; // the list of objects should contain 22 entries / 23 entries depending on the change of Sinotpik
        // as the hours change, sometimes Sinoptik will not produce the prognosis for the next 23 hours, which will result in 22 entries and not 23
        Integer actualSize = hourlyWeather.getHourlyWeatherData().size();

        //Test that the size is either 22 or 23
        assertTrue("Error, random is too high", expectedSizeHigh >= actualSize);
        assertTrue("Error, random is too low", expectedSizeLow <= actualSize);
        System.out.println("---- Testing Hourly Weather Parser ----");
        System.out.println("Expected lower size: " + expectedSizeLow + " ||| Expected higher size: " + expectedSizeHigh + "\nActual size: " + actualSize);
        System.out.println("================================");
        System.out.println();
    }

    // The WeekendWeather object contains an arrayList of <WeekendWeatherData> which should hold 3 objects
    @Test
    public void getWeekendWeather() throws Exception {
        //Prepare
        WeekendWeather weekendWeather = new WeekendWeather();
        int sizeBeforePopulation = weekendWeather.getWeekendWeatherData().size();
        int numberOfObjects = 3;
        String weatherType = "weekend";

        weekendWeather = parser.getWeekendWeather(HTTPConnector.connect(city, weatherType), threadStart);

        // Assert
        Integer expectedSize = sizeBeforePopulation + numberOfObjects; // list must have 3 objects
        Integer actualSize = weekendWeather.getWeekendWeatherData().size();

        Assert.assertEquals(expectedSize, actualSize);
        System.out.println("---- Testing Weekend Weather Parser ----");
        System.out.println("Expected size: " + expectedSize + "\nActual size: " + actualSize);
        System.out.println("================================");
        System.out.println();

    }

    // The FiveDay Weather object contains an arrayList of <FiveDayWeatherData> which is composed of 5 objects
    @Test
    public void getFiveDayWeather() throws IOException, BadRequestException {
        //Prepare
        FiveDayWeather fiveDayWeather = new FiveDayWeather();
        int sizeBeforePopulation = fiveDayWeather.getFiveDayWeatherData().size();
        int numberOfObjects = 5;
        String weatherType = "fiveday";

        fiveDayWeather = parser.getFiveDayWeather(HTTPConnector.connect(city, weatherType), threadStart);

        // Assert
        Integer expectedSize = sizeBeforePopulation + numberOfObjects; // list must have 5 objects
        Integer actualSize = fiveDayWeather.getFiveDayWeatherData().size();

        Assert.assertEquals(expectedSize, actualSize);
        System.out.println("---- Testing Five Day Weather Parser ----");
        System.out.println("Expected size: " + expectedSize + "\nActual size: " + actualSize);
        System.out.println("================================");
        System.out.println();
    }

    // The TenDay Weather object contains an arrayList of <TenDayWeatherData> which is composed of 10 objects
    @Test
    public void getTenDayWeather() throws IOException, BadRequestException {
        //Prepare
        TenDayWeather tenDayWeather = new TenDayWeather();
        int sizeBeforePopulation = tenDayWeather.getTenDayWeatherData().size();
        int numberOfObjects = 10;
        String weatherType = "tenday";

        tenDayWeather = parser.getTenDayWeather(HTTPConnector.connect(city, weatherType), threadStart);

        // Assert
        Integer expectedSize = sizeBeforePopulation + numberOfObjects; // list must have 5 objects
        Integer actualSize = tenDayWeather.getTenDayWeatherData().size();

        Assert.assertEquals(expectedSize, actualSize);
        System.out.println("---- Testing Five Day Weather Parser ----");
        System.out.println("Expected size: " + expectedSize + "\nActual size: " + actualSize);
        System.out.println("================================");
        System.out.println();
    }
}