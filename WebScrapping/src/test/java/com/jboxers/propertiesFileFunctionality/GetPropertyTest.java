package com.jboxers.propertiesFileFunctionality;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class GetPropertyTest {

    @Test
    public void weatherRegexpPattern() throws IOException {
        //Prepare
        String expectedString = GetProperty.weatherRegexpPattern("tenday.weather.full");
        String actualString = "<span.+>(.+)\\s+<.+>(.+)<.+>\\s+<.+\\s+<.+>(\\d+).+<.+>\\s+<.+>.+<.+>\\s+<.+>(\\d+).+\\s+.+>\\s+<.+>\\s+(\\d.\\d+).+\\s+<.+>(\\d+).+<.+>\\s+<.+>(\\d+.\\d+).+\\s+<.+>(\\d+).+<.+>\\s+<.+>(\\d+).+.+\\s+<.+>.+\\s+<.+>.+>\\s+<.+>.+\\s+<.+>.+";

        //Test
        Assert.assertEquals(actualString, expectedString);
        Assert.assertEquals(actualString.length(), expectedString.length());
        System.out.println("--- Testing GetProperty Regex pattern loading ---");
        System.out.println("Expected String: " + expectedString);
        System.out.println("Actual String: " + actualString);
        System.out.println();
        System.out.println("Length of Actual String: " + actualString.length() + " and length of Expected String: " + expectedString.length());
        System.out.println("Are the two strings equal? " + actualString.equals(expectedString));
    }

    @Test
    public void connectionURL() throws IOException {
        // Prepare
        String city = "SOFIA";
        String weatherType = "CURREnt";
        String expectedString = "https://www.sinoptik.bg/sofia-bulgaria-100727011?location";
        String actualString = GetProperty.connectionURL(city, weatherType);

        //Test
        Assert.assertEquals(actualString, expectedString);
        Assert.assertEquals(actualString.length(), expectedString.length());
        System.out.println("--- Testing GetProperty ConnectionURL property loading ---");
        System.out.println("Expected String: " + expectedString);
        System.out.println("Actual String: " + actualString);
        System.out.println();
        System.out.println("Length of Actual String: " + actualString.length() + " and length of Expected String: " + expectedString.length());
        System.out.println("Are the two strings equal? " + actualString.equals(expectedString));

    }
}