package com.jboxers.regexFunctionality;

import com.jboxers.propertiesFileFunctionality.GetProperty;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a singleton class, it's purpose is to shorten the needed code to start matching regular expressions, each time creating a pattern and a matcher
 * The loadPattern method provides convenience and saves you a few lines of repetitive code
 */
public class RegexMatcher {
    private static RegexMatcher groupMatcher;

    static {
        groupMatcher = new RegexMatcher();
    }

    /**
     * @param pattern    - name of the regexFunctionality pattern
     * @param parsedHtml - the HTML parsed by a StringBuilder
     * @return - a matcher ready to be used in the RegexParser class
     * @throws IOException - An exception can occur if no pattern/string builder output is passed
     */
    public static Matcher loadPattern(String pattern, String parsedHtml) throws IOException {
        String regexWeatherPattern = GetProperty.weatherRegexpPattern(pattern);
        Pattern firstPattern = Pattern.compile(regexWeatherPattern);
        return firstPattern.matcher(parsedHtml);
    }


}

