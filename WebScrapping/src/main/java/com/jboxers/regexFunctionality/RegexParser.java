package com.jboxers.regexFunctionality;

import com.jboxers.customExceptions.RegexMatchException;
import com.jboxers.weatherDataModels.*;
import com.jboxers.weatherModels.*;

import java.io.IOException;
import java.util.regex.Matcher;

/**
 * This is a class which is used to traverse through the HTML of Sinoptik.bg and fetch the needed weather
 * information for the specific type: Current, Hourly, Weekend, Five Day, Ten Day Weather
 * Each type of weather is an entity which is stored in the DB.
 * The result from Sinoptik.bg's HTML is given to a string builder and then send back as a String - parsedHtml
 * Regular expressions are used to match and fetch the needed property to create the objects.
 * All of the objects are passed to the EntityHandler class which saves all of the information into a final entity - Forecast
 * The Forecast class contains all of the weather information of the mentioned above weather types
 */
public class RegexParser {

    public RegexParser() {
    }

    /**
     * @param parsedHtml  - HTML of the specified weather location and weather type
     * @param threadStart - start of thread execution
     * @return - a new CurrentWeather object which is then given to the EntityHandler class
     * @throws Exception - If the regex doesn't match an exception will be thrown
     */
    public CurrentWeather getCurrentWeather(String parsedHtml, String threadStart) throws Exception {
        Matcher temperatureRegex = RegexMatcher.loadPattern("current.weather.temperature", parsedHtml);
        Matcher humidityRegex = RegexMatcher.loadPattern("current.weather.humidity", parsedHtml);
        // the below matcher will find all of the remaining properties needed for the CurrentWeather model
        Matcher restOfValuesRegex = RegexMatcher.loadPattern("current.weather.props", parsedHtml);
        if (restOfValuesRegex.find() & temperatureRegex.find() & humidityRegex.find()) {
            int temperature = Integer.parseInt(temperatureRegex.group(1));
            int humidity = Integer.parseInt(humidityRegex.group(1));
            int cloudiness = Integer.parseInt(restOfValuesRegex.group(5));
            int chanceToRain = Integer.parseInt(restOfValuesRegex.group(2));
            double quantityOfRain = Double.parseDouble(restOfValuesRegex.group(3));
            int chanceOfStorm = Integer.parseInt(restOfValuesRegex.group(4));
            double windSpeed = Double.parseDouble(restOfValuesRegex.group(1));
            return new CurrentWeather(new WeatherData(temperature, cloudiness, humidity, chanceToRain, quantityOfRain,
                    chanceOfStorm, windSpeed, threadStart));
        } else {
            throw new RegexMatchException("The Current Weather couldn't be parsed!");
        }
    }

    /**
     * @param parsedHtml  - HTML of the specified weather location and weather type
     * @param threadStart - start of thread execution
     * @return - a new HourlyWeather object which is then given to the EntityHandler class and saved to the DB
     */
    public HourlyWeather getHourlyWeather(String parsedHtml, String threadStart) throws IOException {
        Matcher restOfPropsRegex = RegexMatcher.loadPattern("hourly.weather.props", parsedHtml);
        Matcher hourRegex = RegexMatcher.loadPattern("hourly.weather.hour", parsedHtml);
        HourlyWeather hourlyWeather = new HourlyWeather();

        while (restOfPropsRegex.find() && hourRegex.find()) {

            String hour = hourRegex.group(1);
            int temperature = Integer.parseInt(hourRegex.group(2));
            double windSpeed = Double.parseDouble(restOfPropsRegex.group(1));
            int chanceOfRaining = Integer.parseInt(restOfPropsRegex.group(2));
            double quantityOfRain = Double.parseDouble(restOfPropsRegex.group(3));
            int chanceOfStorm = Integer.parseInt(restOfPropsRegex.group(4));
            int pressure = Integer.parseInt(restOfPropsRegex.group(5));
            int humidity = Integer.parseInt(restOfPropsRegex.group(6));
            int cloudiness = Integer.parseInt(restOfPropsRegex.group(7));


            hourlyWeather.getHourlyWeatherData().add(new HourlyWeatherData(hour, pressure,
                    new WeatherData(temperature, cloudiness, humidity, chanceOfRaining, quantityOfRain, chanceOfStorm, windSpeed, threadStart)));
        }

        return hourlyWeather;
    }

    /**
     * @param parsedHtml  - HTML of the specified weather location and weather type
     * @param threadStart - start of thread execution
     * @return - a new WeekendWeather object which is then given to the EntityHandler class and saved to the DB
     */
    public WeekendWeather getWeekendWeather(String parsedHtml, String threadStart) {
        WeekendWeather weekendWeather = new WeekendWeather();

        try {
            Matcher firstGroups = RegexMatcher.loadPattern("weekend.weather.first", parsedHtml);
            Matcher secondGroups = RegexMatcher.loadPattern("weekend.weather.second", parsedHtml);
            while (firstGroups.find() & secondGroups.find()) {

                String day = firstGroups.group(1);
                String date = firstGroups.group(2);
                int minTemperature = Integer.parseInt(firstGroups.group(3));
                int maxTemperature = Integer.parseInt(secondGroups.group(1));
                double windSpeed = Double.parseDouble(secondGroups.group(2));
                int chanceOfRain = Integer.parseInt(secondGroups.group(3));
                double quantityOfRain = Double.parseDouble(secondGroups.group(4));
                int chanceOfStorm = Integer.parseInt(secondGroups.group(5));
                int cloudiness = Integer.parseInt(secondGroups.group(6));

                weekendWeather.getWeekendWeatherData().add(new WeekendWeatherData(new WeatherData(minTemperature, maxTemperature, cloudiness,
                        chanceOfRain, quantityOfRain, chanceOfStorm, windSpeed, date, threadStart), day));

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return weekendWeather;
    }

    /**
     * @param parsedHtml  - HTML of the specified weather location and weather type
     * @param threadStart - start of thread execution
     * @return - a new FiveDayWeather object which is then given to the EntityHandler class and saved to the DB
     */
    public FiveDayWeather getFiveDayWeather(String parsedHtml, String threadStart) {
        FiveDayWeather fiveDayWeather = new FiveDayWeather();
        try {
            Matcher firstGroups = RegexMatcher.loadPattern("fiveday.weather.firstCapture", parsedHtml);
            Matcher secondGroups = RegexMatcher.loadPattern("fiveday.weather.secondCapture", parsedHtml);

            for (int i = 1; i < 6; i++) {
                //check if regexp is matching through the entire loop
                if (!firstGroups.find() & !secondGroups.find())
                    throw new RegexMatchException("The Five Day regexFunctionality didn't match 5 times!");

                String day = firstGroups.group(1);
                String date = firstGroups.group(2);
                int minimumDegrees = Integer.parseInt(firstGroups.group(3));

                int maximumDegrees = Integer.parseInt(firstGroups.group(4));
                double windSpeed = Double.parseDouble(secondGroups.group(1));
                int chanceOfRaining = Integer.parseInt(secondGroups.group(2));
                double quantityOfRain = Double.parseDouble(secondGroups.group(3));
                int chanceOfStorm = Integer.parseInt(secondGroups.group(4));
                int cloudiness = Integer.parseInt(secondGroups.group(5));

                fiveDayWeather.getFiveDayWeatherData().add(new FiveDayWeatherData(day,
                        new WeatherData(minimumDegrees, maximumDegrees, cloudiness, chanceOfRaining, quantityOfRain, chanceOfStorm, windSpeed, date, threadStart)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (RegexMatchException | NumberFormatException e) {
            e.printStackTrace();
            System.exit(2);
        }
        return fiveDayWeather;
    }

    /**
     * @param parsedHtml  - HTML of the specified weather location and weather type
     * @param threadStart - start of thread execution
     * @return - a new TenDayWeather object which is then given to the EntityHandler class and saved to the DB
     */
    public TenDayWeather getTenDayWeather(String parsedHtml, String threadStart) {
        TenDayWeather tenDayWeather = new TenDayWeather();
        try {
            Matcher tenDayRegex = RegexMatcher.loadPattern("tenday.weather.full", parsedHtml);

            while (tenDayRegex.find()) {
                String day = tenDayRegex.group(1);
                String date = tenDayRegex.group(2);
                int maximumTemperature = Integer.parseInt(tenDayRegex.group(3));
                int minimumTemperature = Integer.parseInt(tenDayRegex.group(4));
                double windSpeed = Double.parseDouble(tenDayRegex.group(5));
                int chanceToRain = Integer.parseInt(tenDayRegex.group(6));
                double quantityOfRain = Double.parseDouble(tenDayRegex.group(7));
                int chanceOfStorm = Integer.parseInt(tenDayRegex.group(8));
                int cloudiness = Integer.parseInt(tenDayRegex.group(9));

                tenDayWeather.getTenDayWeatherData().add(new TenDayWeatherData(new WeatherData(maximumTemperature,
                        minimumTemperature, cloudiness, chanceToRain, quantityOfRain, chanceOfStorm, windSpeed, date, threadStart), day));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tenDayWeather;
    }

}

