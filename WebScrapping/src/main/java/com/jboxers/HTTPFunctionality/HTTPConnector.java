package com.jboxers.HTTPFunctionality;

import com.jboxers.customExceptions.BadRequestException;
import com.jboxers.propertiesFileFunctionality.GetProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HTTPConnector {
    private static HTTPConnector sinoptikHTTPConnection; // Singleton

    static {
        sinoptikHTTPConnection = new HTTPConnector(); //make class Singleton
    }

    /**
     * @param city        - city for which you would like to fetch weather
     * @param weatherType - what kind of forecast - current, hourly, weekend, five day, ten day
     * @return - returns the HTML content of the page as a string
     * @throws IOException         - exception due to String Builder parsing
     * @throws BadRequestException - if status code on connection is not 200
     */
    public static String connect(String city, String weatherType) throws IOException, BadRequestException {
        String url = GetProperty.connectionURL(city.toLowerCase(), weatherType.toLowerCase());
        URL obj = new URL(url);

        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET"); //set to GET request

        String USER_AGENT = "Mozilla/5.0";
        connection.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = connection.getResponseCode(); // get response code

        if (responseCode != 200) {
            throw new BadRequestException("Expected response code 200, but got: " + responseCode);
        }

        BufferedReader input = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
        String inputLine;

        StringBuilder pageSource = new StringBuilder();

        while ((inputLine = input.readLine()) != null) {
            pageSource.append(inputLine).append("\n");
        }
        input.close();
        return pageSource.toString();
    }
}
