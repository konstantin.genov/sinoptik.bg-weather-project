package com.jboxers.HTTPFunctionality;

import com.jboxers.customExceptions.BadRequestException;
import com.jboxers.customExceptions.RegexMatchException;
import com.jboxers.databaseFunctionality.EntityHandler;
import com.jboxers.propertiesFileFunctionality.GetProperty;
import com.jboxers.weatherModels.*;
import com.jboxers.regexFunctionality.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;

public class WeatherFetcher implements Runnable {
    private static final Logger logger = LogManager.getLogger(Class.class.getName());
    private String threadStart;
    private EntityHandler database = new EntityHandler();
    private RegexParser parser = new RegexParser();
    private ArrayList<String> cities = new ArrayList<>();
    private String city;


    public WeatherFetcher(String city) throws Exception {
        this.city = city;
    }

    // TODO: Finish functionality which allows an undetermined amount of cities to be used for weather fetching
    WeatherFetcher(String... city) throws Exception {
        for (String check : city
        ) {
            GetProperty.checkForCity(check);
        }

    }

    public void run() {
        try {
            threadStart = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
            logger.log(Level.INFO, "Starting thread...");
            logger.log(Level.INFO, "Thread started at " + threadStart);

            logger.log(Level.INFO, "Connecting to: Sinoptik.bg ...");

            if (getCurrentWeather("Sofia") != null) logger.log(Level.INFO, "Successfully connected!");

            logger.log(Level.INFO, String.valueOf(getCurrentWeather(city.toLowerCase())));
            logger.log(Level.INFO, String.valueOf(getHourlyWeather(city.toLowerCase())));
            logger.log(Level.INFO, String.valueOf(getWeekendWeather(city.toLowerCase())));
            logger.log(Level.INFO, String.valueOf(getFiveDayWeather(city.toLowerCase())));
            logger.log(Level.INFO, String.valueOf(getTenDayWeather(city.toLowerCase())));


            database.saveWeather(getCurrentWeather("Sofia"), getHourlyWeather("Sofia"), getWeekendWeather
                    ("Sofia"), getFiveDayWeather("Sofia"), getTenDayWeather("Sofia"), threadStart, city);
            logger.log(Level.INFO, "Successfully retrieved weather information and saved to database!.");

            LocalTime time = LocalTime.now();
            logger.log(Level.INFO, "The time now is: " + time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private CurrentWeather getCurrentWeather(String city) throws Exception {
        return parser.getCurrentWeather(HTTPConnector.connect(city, "current"), threadStart);

    }

    private HourlyWeather getHourlyWeather(String city) throws IOException, BadRequestException, RegexMatchException {
        return parser.getHourlyWeather(HTTPConnector.connect(city, "hourly"), threadStart);
    }

    private WeekendWeather getWeekendWeather(String city) throws IOException, BadRequestException {
        return parser.getWeekendWeather(HTTPConnector.connect(city, "weekend"), threadStart);
    }

    private FiveDayWeather getFiveDayWeather(String city) throws IOException, BadRequestException {
        return parser.getFiveDayWeather(HTTPConnector.connect(city, "fiveday"), threadStart);
    }

    private TenDayWeather getTenDayWeather(String city) throws IOException, RegexMatchException, BadRequestException {

        return parser.getTenDayWeather(HTTPConnector.connect(city, "tenday"), threadStart);

    }

}
