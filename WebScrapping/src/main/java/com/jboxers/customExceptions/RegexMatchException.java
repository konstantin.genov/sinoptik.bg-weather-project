package com.jboxers.customExceptions;

public class RegexMatchException extends Exception {
    public RegexMatchException(String message) {
        super(message);
    }
}
