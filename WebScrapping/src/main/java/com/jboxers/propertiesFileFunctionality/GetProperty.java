package com.jboxers.propertiesFileFunctionality;

import com.jboxers.customExceptions.CityNotFoundException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;

/**
 * This class is declared as a Singleton. In order to use it's methods we call it by using GetProperty.{pattern method name}
 * All of the regexFunctionality patterns are loaded through a properties file "weatherConfig.properties" and are extracted by loading them with a UTF-8 InputStreamReader
 * We use the methods in the com.jboxers.HTTPFunctionality.WeatherFetcher class to extract data from the StringBuilder and pass it to primitive types whilst creating new weather objects
 */
public class GetProperty {
    private static GetProperty getProperty; // Singleton
    private static Properties properties = new Properties();

    static {
        getProperty = new GetProperty(); //make class Singleton
    }

    public static String weatherRegexpPattern(String propertyName) throws IOException {
        InputStream regexUTF8 = new FileInputStream("/home/carnivore/Desktop/Git_Repos/jBoxers-Stuff/JavaEE_Project_Composition/weatherConfig.properties");
        properties.load(new InputStreamReader(regexUTF8, StandardCharsets.UTF_8)); // this ensures the file is passed with UTF8
        return (properties.getProperty(propertyName));
    }

    public static String connectionURL(String city, String weatherType) throws IOException {
        InputStream regexUTF8 = new FileInputStream("/home/carnivore/Desktop/Git_Repos/jBoxers-Stuff/JavaEE_Project_Composition/weatherConfig.properties");
        properties.load(new InputStreamReader(regexUTF8, StandardCharsets.UTF_8)); // this ensures the file is passed with UTF8
        return (properties.getProperty("url." + city.toLowerCase() + "." + weatherType.toLowerCase()));
    }

    /**
     * This method will check if a given city is in the properties file.
     * @param city - String of city queried
     * @return - AL of matching cities
     * @throws IOException - if property file is not loaded, exception is thrown
     * @throws CityNotFoundException - the city was not found in the properties file
     */
    public static ArrayList<String> checkForCity(String city) throws IOException, CityNotFoundException {
        InputStream regexUTF8 = new FileInputStream("weatherConfig.properties");
        properties.load(new InputStreamReader(regexUTF8, StandardCharsets.UTF_8)); // this ensures the file is passed with UTF8
        String cities = properties.getProperty("cities");
        String[] splitCities = cities.split(",");
        ArrayList<String> finalCities = new ArrayList<>();
        for (String existingCity : splitCities
        ) {
            if (existingCity.equals(city)) {
                finalCities.add(city);
            } else {
                throw new CityNotFoundException("The city: " + city + "was not found in the configuration");
            }
        }
        return finalCities;
    }

}
