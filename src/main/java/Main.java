import com.jboxers.HTTPFunctionality.WeatherFetcher;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws Exception {


        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new WeatherFetcher("Sofia"), 0, 1, TimeUnit.HOURS);


    }


}
