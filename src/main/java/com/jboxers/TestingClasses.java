package com.jboxers;

import com.jboxers.databaseFunctionality.PersistenceFacade;
import com.jboxers.weatherDTOs.CurrentWeatherDTO;
import com.jboxers.weatherDTOs.DTOMapper;
import com.jboxers.weatherDTOs.FiveDayWeatherDTO;
import com.jboxers.weatherDTOs.ForecastDTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class TestingClasses {
    public static void main(String[] args) {
        final String PERSISTENCE_UNIT_NAME = "my-app";

        // Colors for SOUTing -- pretty printing!
        final String ANSI_RESET = "\u001B[0m";
        final String ANSI_BLACK = "\u001B[30m";
        final String ANSI_RED = "\u001B[31m";
        final String ANSI_GREEN = "\u001B[32m";
        final String ANSI_YELLOW = "\u001B[33m";
        final String ANSI_BLUE = "\u001B[34m";
        final String ANSI_PURPLE = "\u001B[35m";
        final String ANSI_CYAN = "\u001B[36m";
        final String ANSI_WHITE = "\u001B[37m";

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();
        PersistenceFacade persistenceFacade = new PersistenceFacade();

        entityManager.getTransaction().begin();
        entityManager.getTransaction().commit();


        factory.close();

//        List<CurrentWeatherDTO> test = DTOMapper.getCurrentWeatherList("25/07/2019", "25/07/2019 18:13:53");
//        List<HourlyWeatherDTO> hourlyWeatherDTOS = DTOMapper.getHourlyWeatherList("25/07/2019", "18:00");
        List<CurrentWeatherDTO> currentWeatherDTOS = DTOMapper.getCurrentWeatherList("20/07/2019", "22:00");
        List<FiveDayWeatherDTO> fiveDayWeatherDTOS = DTOMapper.getFiveDayWeatherList("27.07.2019");
        List<ForecastDTO> forecastDTOS = DTOMapper.getCurrentVsFivedayForecast("24.07.2019");
        forecastDTOS.forEach(System.out::println);
    }
}

