# Sinoptik.bg Weather Project

This is a Java EE multi-module Maven weather application which takes information from Sinoptik.bg through parsing the HTML files of the given city and forecast type, 
then using regular expressions to extract the needed weather information. 

The resulting objects are stored in an SQL database (MariaDB) with implementations of Java Persistance API (EclipseLink).

2 types of Frontend:
- jQuery, HTML, CSS
- React.js